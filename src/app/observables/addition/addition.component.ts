import { Component, OnInit } from '@angular/core';
import { CalculatorService } from 'src/app/services/calculator/calculator.service';

@Component({
  selector: 'app-addition',
  templateUrl: './addition.component.html',
  styleUrls: ['./addition.component.scss']
})
export class AdditionComponent implements OnInit {

  public additionResult: number = 0;

  constructor(
    private calculatorService: CalculatorService
  ) { }

  ngOnInit(): void {
    this.calculatorService.numbersSubject.subscribe((numbers: any) => {
      this.additionResult = numbers.first + numbers.second
    });
  }
}
