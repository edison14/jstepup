import { Component, OnInit } from '@angular/core';
import { CalculatorService } from 'src/app/services/calculator/calculator.service';

@Component({
  selector: 'app-substraction',
  templateUrl: './substraction.component.html',
  styleUrls: ['./substraction.component.scss']
})
export class SubstractionComponent implements OnInit {

  public subtractionResult: number = 0;

  constructor(private calculatorService: CalculatorService) { }

  ngOnInit(): void {
    this.calculatorService.numbersSubject.subscribe((numbers: any) => {
      this.subtractionResult = numbers.first - numbers.second;
    })
  }

}
