import { Component, OnInit } from '@angular/core';
import { CalculatorService } from '../services/calculator/calculator.service';

@Component({
  selector: 'app-observables',
  templateUrl: './observables.component.html',
  styleUrls: ['./observables.component.scss']
})
export class ObservablesComponent implements OnInit {

  public first: number = 0;
  public second: number = 0;

  constructor(
    private calculatorService: CalculatorService
  ) { }

  ngOnInit(): void {
  }

  /**
   * Publish numbers for operation.
   */
  public submit(): void {
    this.calculatorService.numbersSubject.next({first: this.first, second: this.second});
  }

}
