import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  public numbersSubject = new Subject();

  constructor() { }

  /**
   * Set numbers variable and publish it.
   * 
   * @param numbers 
   */
  public setNumbersSubject(numbers: any): void {
    this.numbersSubject.next(numbers);
  }
}
